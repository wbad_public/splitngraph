const fsPromises = require('fs/promises');
const parseCSV = require('csv-parse');
const path = require('path');
const util = require('util');
const {spawn} = require('child_process');

const depthLimit = 20000;
const parseCSVAsync = util.promisify(parseCSV);

const spawnAsync = async (command, args, options) => {
  return new Promise((resolve, reject) => {
    const stdout = [];
    const stderr = [];
    const subprocess = spawn(command, args, options);
    subprocess.on('close', (code) => {
      if (code != 0) {
        reject({
          command,
          args,
          stdout,
          stderr,
          exitCode: code
        });
        return;
      }
      resolve({
        command,
        args,
        stdout,
        stderr,
        exitCode: code
      });
    })
    subprocess.stdout.on('data', (data) => {
      stdout.push(data);
    });
    subprocess.stderr.on('data', (data) => {
      stderr.push(data);
    });
  });
};
const fromEdgeAddNodes = (nodeMap, [parentKey, childKey]) => {
  const getNode = (key) => {
    if(!nodeMap.has(key)) {
      const node = {
        key,
        childSet: new Set(),
        parentSet: new Set()
      };
      nodeMap.set(key, node);
    }
    return nodeMap.get(key);
  };
  
  const parentNode = getNode(parentKey);
  const childNode = getNode(childKey);

  parentNode.childSet.add(childNode);
  childNode.parentSet.add(parentNode);
  return nodeMap;
};
const getEdgeList = (node, accumulator=[], visitSet=new Set(), depth=0) => {
  if(depth > depthLimit) {
    console.log(`WE HAVE CYCLES!!!`);
    process.exit(-1);
  }
  const nodeKey = node.key;
  node.childSet.forEach((childNode) => {
    const childKey = childNode.key;
    const edgeKey = `${nodeKey}<-->${childKey}`;
    if(!visitSet.has(edgeKey)) {
      accumulator.push([nodeKey, childKey]);
      visitSet.add(edgeKey);
    }
  });
  node.childSet.forEach((childNode) => {
    accumulator = getEdgeList(childNode, accumulator, visitSet, depth + 1);
  });
  return accumulator;
};

const doIt = async ({
  inputPath,
  outputPath,
  dotExecPath,
}) => {
  const raw = await fsPromises.readFile(inputPath, 'utf8');
  const parsed = await parseCSVAsync(raw, {comment: '#'});

  const nodeMap = parsed
    .slice(1)
    .reduce(fromEdgeAddNodes, new Map());
  const rootNodes = [...nodeMap.values()].filter(
    (node) => {
      const hasParent = node.parentSet.size > 0;;
      return !hasParent;
    }
  );
  console.log(`rootNodes: ${rootNodes.length}`);

  await fsPromises.mkdir(outputPath, {recursive: true});
  const splitDotFiles = await Promise.all(
    rootNodes.map(
      async (rootNode) => {
        const edgeList = getEdgeList(rootNode);
        const outputList = [
          `digraph foo {`,
          '  node [color=lightblue2, style=filled];',
          ...edgeList.map(
            ([parentKey, childKey]) => `  "${parentKey}" -> "${childKey}";`
          ),
          '  overlap=false;',
          '  fontsize=12;',
          '}'
        ];

        const outputFilePath = path.resolve(outputPath, `${rootNode.key.replace(/[\/:]/g, '_')}.dot`);
        await fsPromises.writeFile(outputFilePath, outputList.join('\n'), 'utf8');
        return {
          key: rootNode.key,
          layout: 'circo',
          // layout: edgeList.length < 100
          //   ?  'neato'
          //   : (
          //     edgeList.length < 1000
          //       ? 'twopi'
          //       : 'circo'
          //   ),
          outputFilePath
        };
      }
    )
  );

  await Promise.allSettled(
    splitDotFiles.map(
      ({key, layout, outputFilePath}) => {
        const result = spawnAsync(
          dotExecPath,
          [`-K${layout}`, '-O', '-Tsvg', outputFilePath],
          {cwd: outputPath, encoding: 'utf8'}
        );
        return result;
      }
    )
  )
  .then((settledList) => {
    const failed = settledList.filter(
      (settled) => settled.status === 'rejected'
    );
    if (failed.length > 0) {
      throw failed[0].reason;
    }
  })
  .then(() => {
    console.log('Done!!');
  })
  .catch((e) => {
    console.error(`ERROR!!\nCommand: ${e.command}\nArgs: ${e.args}\nExit Code: ${e.exitCode}\nStdOut: ${JSON.stringify(e.stdout)}\nStdErr: ${JSON.stringify(e.stderr)}`);
    process.exit(-1);
  });
};

doIt({
  inputPath: path.resolve('./BOM.csv'),
  outputPath: path.resolve('./Bom'),
  dotExecPath: path.resolve('C:\\Users\\werner\\scoop\\shims\\dot.exe')
});
